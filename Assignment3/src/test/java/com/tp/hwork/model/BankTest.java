package com.tp.hwork.model;

import org.junit.Test;
import java.util.ArrayList;
import static org.junit.Assert.*;

public class BankTest {

    @Test
    public void insertPerson() {
        Bank bank = new Bank();
        Person person = new Person("Malnas", "Lorand", "1981209123453", "malnaslori23@gamil.com", "0740232323");
        assert bank.insertPerson(person) : "Unable to insert  new Person";
    }

    @Test
    public void updatePerson() {
        Bank bank = new Bank();
        Person oldPerson = new Person("Malnas", "Lorand", "1981209123453", "malnaslori23@gamil.com", "0740232323");
        assert bank.insertPerson(oldPerson) : "Unable to insert  new Person";
        Person newPerson = new Person("Malnas", "Ferenc", "1981209123453", "malnaslori23@gamil.com","0740909090");
        assert bank.updatePerson(oldPerson, newPerson) : "Unable to update the person";
    }

    @Test
    public void deletePerson() {
        Bank bank = new Bank();
        Person person = new Person("Malnas", "Lorand", "1981209123453", "malnaslori23@gamil.com", "0740232323");
        assert bank.insertPerson(person) : "Unable to insert new Person";
        assert bank.deletePerson(person) : "Unable to delete the person";
    }

    @Test
    public void viewAllPerson() {
        Bank bank = new Bank();
        ArrayList<Person> people = new ArrayList<>();
        people.add(new Person("Jakab", "Gyongyi", "2971023129876", "jakabgyongyi8@gmail.com", "0740989898"));
        people.add(new Person("Dobner", "Imola", "2970824099876", "dobner.imo@yahoo.com", "0740956798"));
        people.add(new Person("Jonas", "Gyorgy", "1891209123456", "jonasigy23@gmail.com", "0740989111"));
        for(Person itr: people){
            assert bank.insertPerson(itr) : "Unable to insert new person";
        }
        assert bank.viewAllPerson() != null : "Unable to view people";
    }

    @Test
    public void insertAccount() {
        Bank bank = new Bank();
        Person person = new Person("Malnas", "Lorand", "1981209123453", "malnaslori23@gamil.com", "0740232323");
        assert bank.insertPerson(person) : "Unable to insert new person";
        Account account = new SavingAccount("0", 0.05);
        account.setTotalSum(2300);
        assert bank.insertAccount(person, account) : "Unable to insert new saving account";
        account = new SpendingAccount("1", 0.01);
        assert bank.insertAccount(person, account): "Unable to insert new spending account";

    }

    @Test
    public void updateAccount() {
        Bank bank = new Bank();
        Person person = new Person("Malnas", "Lorand", "1981209123453", "malnaslori23@gamil.com", "0740232323");
        assert bank.insertPerson(person) : "Unable to insert new person";
        Account oldAccount = new SavingAccount("0", 0.05);
        oldAccount.setTotalSum(2300);
        assert bank.insertAccount(person, oldAccount) : "Unable to insert new saving account";
        Account newAccount = new SavingAccount("0", 0.06);
        assert bank.updateAccount(oldAccount, newAccount);
    }

    @Test
    public void deleteAccount() {
        Bank bank = new Bank();
        Person person = new Person("Malnas", "Lorand", "1981209123453", "malnaslori23@gamil.com", "0740232323");
        assert bank.insertPerson(person) : "Unable to insert new person";
        Account account = new SavingAccount("0", 0.05);
        account.setTotalSum(2300);
        assert bank.insertAccount(person, account) : "Unable to insert saving account";
        assert bank.deleteAccount(account) : "Unable to delete saving account";
    }

    @Test
    public void viewAllAccount() {
    }
}