package com.tp.hwork.controller;

import com.tp.hwork.model.Bank;
import com.tp.hwork.model.Person;
import com.tp.hwork.validator.PersonValidator;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;


public class PersonController {
    private PersonValidator personValidator;
    private Bank bank;

    public PersonController(Bank bank){
        personValidator = new PersonValidator();
        this.bank = bank;
    }

    public boolean insertPerson(TextField lastName, TextField firstName, TextField ssn, TextField email, TextField telnum, TableView<Person> personTable){
        boolean okay = true;
        Person person = new Person();
        if(personValidator.lastNameValidation(lastName) == 1)
            person.setLastName(lastName.getText());
        else
            okay = false;
        if(personValidator.firstNameValidation(firstName) == 1)
            person.setFirstName(firstName.getText());
        else
            okay = false;
        if(personValidator.ssnValidation(ssn) == 1)
            person.setSocialSecurityNumber(ssn.getText());
        else
            okay = false;
        if(personValidator.emailValidation(email) == 1)
            person.setEmail(email.getText());
        else
            okay = false;
        if(personValidator.telNumberValidation(telnum) == 1)
            person.setTelNumber(telnum.getText());
        else
            okay = false;
        if(okay){
            if(bank.insertPerson(person)) {
                System.out.println("We inserted a new person\n"+person);
                personTable.setItems(bank.viewAllPerson());
                personTable.refresh();
            }
            return true;
        }
        return false;
    }

    public boolean updatePerson(TextField lastName, TextField firstName, TextField ssn, TextField email, TextField telnum, TableView<Person> personTable){
        Person selectedPerson = personTable.getSelectionModel().getSelectedItem();
        if(selectedPerson !=  null) {
            System.out.println("Selected Person:\n"+selectedPerson);

            Person person = new Person();
            int answer = personValidator.lastNameValidation(lastName);
            if(answer == 1)
                person.setLastName(lastName.getText());
            else if(answer == 0)
                person.setLastName(selectedPerson.getLastName());

            answer = personValidator.firstNameValidation(firstName);
            if(answer == 1)
                person.setFirstName(firstName.getText());
            else if(answer == 0)
                person.setFirstName(selectedPerson.getFirstName());

            answer = personValidator.ssnValidation(ssn);
            if(answer == 1)
                person.setSocialSecurityNumber(ssn.getText());
            else if(answer == 0)
                person.setSocialSecurityNumber(selectedPerson.getSocialSecurityNumber());

            answer = personValidator.emailValidation(email);
            if(answer == 1)
                person.setEmail(email.getText());
            else if(answer == 0)
                person.setEmail(selectedPerson.getEmail());

            answer = personValidator.telNumberValidation(telnum);
            if(answer == 1)
                person.setTelNumber(telnum.getText());
            else if(answer == 0)
                person.setTelNumber(selectedPerson.getTelNumber());

            if(bank.updatePerson(selectedPerson, person)) {
                System.out.println("We updated to person\n"+person);
                personTable.setItems(bank.viewAllPerson());
                personTable.refresh();
                return true;
            }
        }
        return false;
    }

    public void deletePerson(TableView<Person> personTable){
        Person selectedPerson = personTable.getSelectionModel().getSelectedItem();
        if(selectedPerson != null){
            System.out.println("Selected Person:\n"+selectedPerson);
            if(bank.deletePerson(selectedPerson)){
                System.out.println("We deleted ath selected peron\n");
                personTable.setItems(bank.viewAllPerson());
                personTable.refresh();
            }
        }
    }
}
