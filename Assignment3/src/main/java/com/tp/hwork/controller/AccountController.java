package com.tp.hwork.controller;

import com.tp.hwork.model.*;
import com.tp.hwork.validator.AccountValidator;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import java.util.Random;


public class AccountController {
    private AccountValidator accountValidator;
    private Bank bank;

    public AccountController(Bank bank){
        accountValidator = new AccountValidator();
        this.bank = bank;
    }

    public boolean insertSavingAccount(ChoiceBox<String> ssn, TextField interest, TextField sum, TableView<SavingAccount> savingAccountTable){
        if(accountValidator.interestValidation(interest.getText()) == 1){
            SavingAccount savingAccount = new SavingAccount(generateIBAN(ssn.getValue()), Double.parseDouble(interest.getText()));
            savingAccount.setTotalSum(Double.parseDouble(sum.getText()));
            Person person = bank.findBySSN(ssn.getValue());
            if(bank.insertAccount(person, savingAccount)){
                System.out.println("We inserted a Saving Account:\n"+savingAccount);
                savingAccountTable.setItems(bank.viewAllSavingAccount());
                savingAccountTable.refresh();
                return true;
            }
        }
        return false;
    }

    public boolean insertSpendingAccount(ChoiceBox<String> ssn, TextField withdrawloss, TableView<SpendingAccount> spendingAccountTable) {
        if(accountValidator.withdrawLossValidation(withdrawloss.getText()) == 1){
            SpendingAccount spendingAccount = new SpendingAccount(generateIBAN(ssn.getValue()), Double.parseDouble(withdrawloss.getText()));
            Person person = bank.findBySSN(ssn.getValue());
            if(bank.insertAccount(person, spendingAccount)){
                System.out.println("We inserted a Spending Account:\n"+spendingAccount);
                spendingAccountTable.setItems(bank.viewAllSpendingAccount());
                spendingAccountTable.refresh();
                return true;
            }
        }
        return false;
    }

    public boolean updateSavingAccount(TextField interest, TableView<SavingAccount> savingAccountTable){
        SavingAccount selectedItem = savingAccountTable.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            SavingAccount newAccount = new SavingAccount();
            newAccount.setID(selectedItem.getID());
            newAccount.setTotalSum(selectedItem.getTotalSum());
            if(accountValidator.interestValidation(interest.getText()) == 1)
                newAccount.setInterest(Double.parseDouble(interest.getText()));
            else
                newAccount.setInterest(selectedItem.getInterest());
            if(bank.updateAccount(selectedItem, newAccount)){
                System.out.println("We updated Saving Account:\n"+selectedItem+" to Saving Account:\n"+newAccount);
                savingAccountTable.setItems(bank.viewAllSavingAccount());
                savingAccountTable.refresh();
                return true;
            }
        }
        return false;
    }

    public boolean updateSpendingAccount(TextField withdrawloss, TableView<SpendingAccount> spendingAccountTable){
        SpendingAccount selectedItem = spendingAccountTable.getSelectionModel().getSelectedItem();
        if(selectedItem != null) {
            SpendingAccount newAccount = new SpendingAccount();
            newAccount.setID(selectedItem.getID());
            newAccount.setTotalSum(selectedItem.getTotalSum());
            newAccount.setTransactions(selectedItem.getTransactions());
            if (accountValidator.withdrawLossValidation(withdrawloss.getText()) == 1)
                newAccount.setWithDrawLoss(Double.parseDouble(withdrawloss.getText()));
            else
                newAccount.setWithDrawLoss(selectedItem.getWithDrawLoss());
            if (bank.updateAccount(selectedItem, newAccount)) {
                System.out.println("We updated Spending Account:\n" + selectedItem + " to Spending Account:\n" + newAccount);
                spendingAccountTable.setItems(bank.viewAllSpendingAccount());
                spendingAccountTable.refresh();
                return true;
            }
        }
        return false;
    }

    public void deleteSavingAccount(TableView<SavingAccount> savingAccountTable){
        SavingAccount selectedItem = savingAccountTable.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            if(bank.deleteAccount(selectedItem)){
                System.out.println("We successfully deleted the Saving Account:\n"+selectedItem);
                savingAccountTable.setItems(bank.viewAllSavingAccount());
                savingAccountTable.refresh();
            }
        }
    }

    public void deleteSpendingAccount(TableView<SpendingAccount> spendingAccountTable){
        SpendingAccount selectedItem = spendingAccountTable.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            if(bank.deleteAccount(selectedItem)){
                System.out.println("We successfully deleted the Spending Account:\n"+selectedItem);
                spendingAccountTable.setItems(bank.viewAllSpendingAccount());
                spendingAccountTable.refresh();
            }
        }
    }

    public void addTransaction(ChoiceBox<String> providerID, ChoiceBox<String> recipientID, TextField quantity){
        if(accountValidator.quantityValidation(quantity.getText()) == 1){
            System.out.println("Ready for transaction creation between "+ providerID.getValue()+" and "+recipientID.getValue()+" of sum: "+quantity.getText());
            Transaction transaction = new Transaction(providerID.getValue(), recipientID.getValue(), Double.parseDouble(quantity.getText()));
            bank.addTransaction(transaction);
        }
    }

    private String generateIBAN(String ssn){
        Random rand = new Random();
        StringBuilder sb = new StringBuilder();
        sb.append("RO").append(rand.nextInt(10)).append(rand.nextInt(10));
        sb.append("ECRL").append(ssn).append("EURO");
        sb.append(System.currentTimeMillis()).append("XX");
        return sb.toString();
    }
}


