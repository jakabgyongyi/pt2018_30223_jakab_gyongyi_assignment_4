package com.tp.hwork;

import com.tp.hwork.model.Bank;
import javafx.application.Application;
import javafx.stage.Stage;
import com.tp.hwork.view.BankManager;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Main extends Application {
    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Bank bank;
        try{
            FileInputStream fileIn = new FileInputStream("Database.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            bank = (Bank)in.readObject();
            fileIn.close();
        }catch(IOException i){
            i.printStackTrace();
            bank = new Bank();
        }catch(ClassNotFoundException c){
            System.out.println("Bank class not found");
            c.printStackTrace();
            return;
        }

        BankManager bm = new BankManager(bank);
        bm.handleActionEvent();
    }
}
