package com.tp.hwork.view;

import com.tp.hwork.model.Bank;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.*;

public class BankManager {
    private Bank bank;
    private Stage window;
    private Button clientLogin;
    private Button adminLogin;
    private ImageView backGround;

    public BankManager(Bank bank){
        this.bank = bank;
        window = new Stage();
        window.setTitle("Bank Manager");
        AnchorPane layout = new AnchorPane();
        initialize(layout);
        Scene root = new Scene(layout, 900, 600);
        root.getStylesheets().add("DarkTheme.css");
        window.setScene(root);
        window.show();
    }

    private void initialize(AnchorPane layout){
        backGround= new ImageView(new Image("cluj.png"));
        layout.getChildren().addAll(backGround);
        clientLogin = new Button("Log In as Client");
        clientLogin.setLayoutX(100);
        clientLogin.setLayoutY(50);
        adminLogin = new Button("Access Admin Profile");
        adminLogin.setLayoutX(100);
        adminLogin.setLayoutY(100);
        layout.getChildren().addAll(clientLogin, adminLogin);
    }

    public void handleActionEvent(){
        clientLogin.setOnAction(e->{
            ClientLogin cl = new ClientLogin();
            cl.handleActionEvent();
        });
        adminLogin.setOnAction(e->{
            AdminAccount aa = new AdminAccount(bank);
            aa.handleActionEvent();
        });
        window.setOnCloseRequest(e->{
            try {
                FileOutputStream fileOut = new FileOutputStream("Database.ser");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(bank);
                out.close();
                fileOut.close();
                System.out.println("Serialized data is saved in Database.ser");
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        });
    }
}
