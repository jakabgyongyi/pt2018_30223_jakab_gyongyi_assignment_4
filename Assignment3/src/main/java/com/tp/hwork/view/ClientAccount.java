package com.tp.hwork.view;

import com.tp.hwork.model.Person;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

public class ClientAccount {
    private Stage window;
    private Person person;

    public ClientAccount(Person person){
        this.person = person;
        window = new Stage();
        window.setTitle("Account holder: "+person.getLastName()+" "+person.getFirstName());
        TabPane layout = new TabPane();
        Scene scene =  new Scene(layout, 1200, 800);
        scene.getStylesheets().add("DarkTheme.css");
        window.setScene(scene);
        window.show();
    }

    private void initializeNotificationAccount(TabPane layout){

    }

    private void initializeAccount(TabPane layout){

    }

    private void initializePersonalInformation(TabPane layout){

    }

    private void initializeTransaction(TabPane layout){

    }
}
