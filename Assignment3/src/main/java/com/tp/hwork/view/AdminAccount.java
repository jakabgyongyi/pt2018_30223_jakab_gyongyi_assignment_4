package com.tp.hwork.view;

import com.tp.hwork.controller.AccountController;
import com.tp.hwork.controller.PersonController;
import com.tp.hwork.model.*;
import com.tp.hwork.validator.AccountValidator;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.sql.Timestamp;

public class AdminAccount {
    private Bank bank;
    private PersonController personController;
    private AccountController accountController;
    private Stage window;
    private TableView<Person> personTable;
    private TableView<SavingAccount> savingAccountTable;
    private TableView<SpendingAccount> spendingAccountTable;
    private TableView<Transaction> transactionTable;
    private TextField lastName;
    private TextField firstName;
    private TextField ssn;
    private TextField email;
    private TextField telnum;
    private Button addPerson;
    private Button editPerson;
    private Button deletePerson;

    private BorderPane bp;
    private ChoiceBox<String> IDChoice;
    private TextField totalSum;
    private TextField interestORwithdraw;
    private CheckBox accountType;
    private Label accountTypeName;
    private Label iORwLabel;
    private Button addAccount;
    private Button editAccount;
    private Button deleteAccount;
    private Button addMoney;
    private Button withDrawmoney;

    private ChoiceBox<String> providerCh;
    private ChoiceBox<String> recipientCH;
    private TextField quantity;
    private Button addTransaction;
    private Button filterTransactions;

    public AdminAccount(Bank bank){
        this.bank = bank;
        this.personController = new PersonController(bank);
        this.accountController = new AccountController(bank);
        window = new Stage();
        window.setTitle("Admin Account");
        TabPane layout = new TabPane();
        initializeTabAccount(layout);
        initializeTabPerson(layout);
        Scene root = new Scene(layout, 1100, 700);
        root.getStylesheets().add("DarkTheme.css");
        window.setScene(root);
        window.show();
    }

    private void initializeTabPerson(TabPane layout){
        Tab personTab = new Tab("Person Manager");
        personTab.setClosable(false);
        layout.getTabs().add(personTab);
        BorderPane bp = new BorderPane();
        initializePersonLeftMenu(bp);
        initializePersonRightMenu(bp);
        initializePersonBottomMenu(bp);
        personTab.setContent(bp);
    }

    private void initializePersonLeftMenu(BorderPane layout){
        VBox left = new VBox();
        left.setPadding(new Insets(20, 10, 20, 10));
        left.setAlignment(Pos.TOP_CENTER);
        layout.setLeft(left);
        personTable = new TableView<>();
        initializePersonTable();
        left.getChildren().addAll(personTable);
    }

    private void initializePersonTable(){
        TableColumn<Person, String> lastCol = new TableColumn<>("Last Name");
        lastCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        lastCol.setMinWidth(150);
        TableColumn<Person, String> firstCol = new TableColumn<>("First Name");
        firstCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        firstCol.setMinWidth(150);
        TableColumn<Person, String> ssnCol = new TableColumn<>("SocialSecNumber");
        ssnCol.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        ssnCol.setMinWidth(150);
        TableColumn<Person, String> emailCol = new TableColumn<>("Email");
        emailCol.setCellValueFactory(new PropertyValueFactory<>("email"));
        emailCol.setMinWidth(200);
        TableColumn<Person, String> telNumCol = new TableColumn<>("Phone Number");
        telNumCol.setCellValueFactory(new PropertyValueFactory<>("telNumber"));
        telNumCol.setMinWidth(150);
        personTable.getColumns().addAll(lastCol, firstCol, ssnCol, emailCol, telNumCol);
        ObservableList<Person> people = bank.viewAllPerson();
        personTable.setItems(people);
        personTable.refresh();

    }

    private void initializePersonRightMenu(BorderPane layout){
        VBox right = new VBox();
        right.setPadding(new Insets(20, 20, 10, 20));
        right.setSpacing(10);
        right.setAlignment(Pos.TOP_CENTER);
        layout.setRight(right);
        lastName = new TextField();
        lastName.setPromptText("Jakab");
        lastName.setMaxWidth(250);
        firstName = new TextField();
        firstName.setMaxWidth(250);
        firstName.setPromptText("Gyongyi Aniko");
        ssn = new TextField();
        ssn.setPromptText("2971023001234");
        ssn.setMaxWidth(250);
        email = new TextField();
        email.setPromptText("jakabhehe@email.com");
        email.setMaxWidth(250);
        telnum = new TextField();
        telnum.setPromptText("0740909090");
        telnum.setMaxWidth(250);
        right.getChildren().addAll(new Label("Last Name"), lastName, new Label("First Name"), firstName, new Label("Social Security Number"), ssn, new Label("Email"), email, new Label("Phone Number"), telnum);
    }

    private void initializePersonBottomMenu(BorderPane layout){
        HBox bottom = new HBox();
        bottom.setAlignment(Pos.TOP_CENTER);
        bottom.setPadding(new Insets(10, 20, 20, 10));
        bottom.setSpacing(30);
        addPerson = new Button("Add Person");
        editPerson = new Button("Edit Person");
        deletePerson = new Button("Delete Person");
        bottom.getChildren().addAll(addPerson, editPerson, deletePerson);
        layout.setBottom(bottom);
    }

    private void initializeTabAccount(TabPane layout){
        Tab accountTab = new Tab("Account Manager");
        accountTab.setClosable(false);
        layout.getTabs().add(accountTab);
        bp = new BorderPane();
        accountTab.setContent(bp);
        initializeAccountBottomMenu(bp);
        initializeSavingAccountLeft(bp);
        initializeAccountTopMenu(bp);
        initializeSavingAccountRight(bp);
    }

    private void initializeAccountBottomMenu(BorderPane layout){
        HBox bottom = new HBox();
        bottom.setAlignment(Pos.TOP_CENTER);
        bottom.setPadding(new Insets(10, 20, 20, 10));
        bottom.setSpacing(30);
        layout.setBottom(bottom);
        addAccount = new Button("Add Account");
        editAccount = new Button("Edit Account");
        deleteAccount = new Button("Delete Account");
        bottom.getChildren().addAll(addAccount, editAccount, deleteAccount);
    }

    private void initializeSavingAccountLeft(BorderPane layout){
        savingAccountTable = new TableView<>();
        TableColumn<SavingAccount, String> idCol = new TableColumn<>("IBAN");
        idCol.setCellValueFactory(new PropertyValueFactory<>("ID"));
        idCol.setMinWidth(250);
        TableColumn<SavingAccount, Timestamp> timeCol = new TableColumn<>("Time of creation");
        timeCol.setCellValueFactory(new PropertyValueFactory<>("creationTime"));
        timeCol.setMinWidth(200);
        TableColumn<SavingAccount, Double> totalSumCol = new TableColumn<>("Total Content");
        totalSumCol.setCellValueFactory(new PropertyValueFactory<>("TotalSum"));
        totalSumCol.setMinWidth(150);
        TableColumn<SavingAccount, Double> interestCol = new TableColumn<>("Interest(%)");
        interestCol.setCellValueFactory(new PropertyValueFactory<>("interest"));
        interestCol.setMinWidth(100);
        savingAccountTable.getColumns().addAll(idCol, timeCol, totalSumCol, interestCol);
        savingAccountTable.setItems(bank.viewAllSavingAccount());
        savingAccountTable.refresh();

        VBox left = new VBox();
        left.setAlignment(Pos.TOP_CENTER);
        left.setPadding(new Insets(30, 20, 30, 20));
        left.setSpacing(30);
        left.getChildren().add(savingAccountTable);
        layout.setLeft(left);
    }

    private void initializeSpendingAccountLeft(BorderPane layout){
        spendingAccountTable = new TableView<>();
        TableColumn<SpendingAccount, String> idCol = new TableColumn<>("IBAN");
        idCol.setCellValueFactory(new PropertyValueFactory<>("ID"));
        idCol.setMinWidth(250);
        TableColumn<SpendingAccount, Timestamp> timeCol = new TableColumn<>("Time of creation");
        timeCol.setCellValueFactory(new PropertyValueFactory<>("creationTime"));
        timeCol.setMinWidth(200);
        TableColumn<SpendingAccount, Double> totalSumCol = new TableColumn<>("Total Content");
        totalSumCol.setCellValueFactory(new PropertyValueFactory<>("TotalSum"));
        totalSumCol.setMinWidth(150);
        TableColumn<SpendingAccount, Double> withDrawLossCol = new TableColumn<>("WithdrawLoss(%)");
        withDrawLossCol.setCellValueFactory(new PropertyValueFactory<>("withDrawLoss"));
        withDrawLossCol.setMinWidth(100);
        spendingAccountTable.getColumns().addAll(idCol, timeCol, totalSumCol, withDrawLossCol);
        spendingAccountTable.setItems(bank.viewAllSpendingAccount());
        spendingAccountTable.refresh();

        transactionTable = new TableView<>();
        TableColumn<Transaction, String> providerCol = new TableColumn<>("Provider");
        providerCol.setCellValueFactory(new PropertyValueFactory<>("providerID"));
        providerCol.setMinWidth(150);
        TableColumn<Transaction, String> recipientCol = new TableColumn<>("Recipient");
        recipientCol.setCellValueFactory(new PropertyValueFactory<>("recipientID"));
        recipientCol.setMinWidth(150);
        TableColumn<Transaction, Double> quantityCol = new TableColumn<>("Quantity");
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        quantityCol.setMinWidth(100);
        TableColumn<Transaction, Timestamp> time2Col = new TableColumn<>("time");
        time2Col.setCellValueFactory(new PropertyValueFactory<>("time"));
        time2Col.setMinWidth(100);
        TableColumn<Transaction, String> descriptionCol = new TableColumn<>("Description");
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));
        descriptionCol.setMinWidth(200);
        transactionTable.getColumns().addAll(providerCol, recipientCol, quantityCol, time2Col, descriptionCol);
        transactionTable.setItems(null);
        transactionTable.refresh();

        VBox left = new VBox();
        left.setAlignment(Pos.TOP_CENTER);
        left.setPadding(new Insets(30, 20, 30, 20));
        left.setSpacing(30);
        left.getChildren().addAll(spendingAccountTable, transactionTable);
        layout.setLeft(left);

    }

    private void initializeAccountTopMenu(BorderPane layout){
        accountType = new CheckBox();
        accountType.setSelected(true);
        accountTypeName = new Label("Saving Account");
        HBox top = new HBox();
        top.setPadding(new Insets(30, 10, 0, 30));
        top.setSpacing(10);
        top.setAlignment(Pos.CENTER);
        top.getChildren().addAll(accountType, accountTypeName);
        layout.setTop(top);
    }

    private void initializeSavingAccountRight(BorderPane layout){
        IDChoice = new ChoiceBox<>();
        IDChoice.setItems(bank.viewAllSocialSecurityNumber());
        totalSum = new TextField();
        interestORwithdraw = new TextField();
        iORwLabel = new Label("Interest");
        addTransaction = new Button("Add Transaction");
        filterTransactions = new Button("Filter Transactons");
        addMoney = new Button("Add Money");
        withDrawmoney = new Button("WithDraw Money");

        VBox right = new VBox();
        right.setPadding(new Insets(30, 10, 10, 30));
        right.setSpacing(20);
        right.setAlignment(Pos.CENTER);
        right.getChildren().addAll(new Label("Social Security Number"), IDChoice, new Label("Total Content"), totalSum, iORwLabel, interestORwithdraw, addMoney, withDrawmoney);
        layout.setRight(right);
    }

    private void initializeSpendingAccountRight(BorderPane layout){
        iORwLabel.setText("Withdraw Loss");
        providerCh = new ChoiceBox<>();
        providerCh.setMaxWidth(200);
        providerCh.setItems(bank.getAllIBAN("SPENDING"));
        recipientCH = new ChoiceBox<>();
        recipientCH.setMaxWidth(200);
        recipientCH.setItems(bank.getAllIBAN("SPENDING"));
        quantity = new TextField();

        VBox right = new VBox();
        right.setPadding(new Insets(10, 10, 10, 10));
        right.setSpacing(10);
        right.setAlignment(Pos.CENTER);
        right.getChildren().addAll(new Label("Social Security Number"), IDChoice,iORwLabel, interestORwithdraw);
        right.getChildren().addAll(new Label("Provider IBAN"), providerCh, new Label("Recipient IBAN"), recipientCH, new Label("Quantity"), quantity, addTransaction, filterTransactions);
        layout.setRight(right);
    }

    public void handleActionEvent(){
        addPerson.setOnAction(e->{
            if(personController.insertPerson(lastName, firstName, ssn, email, telnum, personTable)) {
                resetPerson();
                IDChoice.setItems(bank.viewAllSocialSecurityNumber());
                IDChoice.setValue(IDChoice.getItems().get(0));
            }
        });
        editPerson.setOnAction(e->{
            if(personController.updatePerson(lastName, firstName, ssn, email, telnum, personTable)) {
                resetPerson();
                IDChoice.setItems(bank.viewAllSocialSecurityNumber());
                IDChoice.setValue(IDChoice.getItems().get(0));
            }
        });
        deletePerson.setOnAction(e->{
            personController.deletePerson(personTable);
            IDChoice.setItems(bank.viewAllSocialSecurityNumber());
            IDChoice.setValue(IDChoice.getItems().get(0));
        });
        addAccount.setOnAction(e->{
            if(accountType.isSelected()) {
                if (accountController.insertSavingAccount(IDChoice, interestORwithdraw, totalSum, savingAccountTable))
                    resetSaving();
            }
            else if(accountController.insertSpendingAccount(IDChoice, interestORwithdraw, spendingAccountTable)) {
                    providerCh.setItems(bank.getAllIBAN("SPENDING"));
                    providerCh.setValue(providerCh.getItems().get(0));
                    recipientCH.setItems(bank.getAllIBAN("SPENDING"));
                    recipientCH.setValue(recipientCH.getItems().get(0));
                    resetSpending();
                }
        });
        editAccount.setOnAction(e->{
            if(accountType.isSelected()) {
                if (accountController.updateSavingAccount(interestORwithdraw, savingAccountTable))
                    resetSaving();
            }
            else if(accountController.updateSpendingAccount(interestORwithdraw, spendingAccountTable)){
                    providerCh.setItems(bank.getAllIBAN("SPENDING"));
                    providerCh.setValue(providerCh.getItems().get(0));
                    recipientCH.setItems(bank.getAllIBAN("SPENDING"));
                    recipientCH.setValue(recipientCH.getItems().get(0));
                    resetSpending();
                }
        });
        deleteAccount.setOnAction(e->{
            if(accountType.isSelected())
               accountController.deleteSavingAccount(savingAccountTable);
            else {
                accountController.deleteSpendingAccount(spendingAccountTable);
                providerCh.setItems(bank.getAllIBAN("SPENDING"));
                providerCh.setValue(providerCh.getItems().get(0));
                recipientCH.setItems(bank.getAllIBAN("SPENDING"));
                recipientCH.setValue(recipientCH.getItems().get(0));
            }
            resetSpending();
            resetSaving();
        });
        addTransaction.setOnAction(e->{
            System.out.println("Add transaction");
            accountController.addTransaction(providerCh, recipientCH, quantity);
            spendingAccountTable.setItems(bank.viewAllSpendingAccount());
            spendingAccountTable.refresh();
        });
        filterTransactions.setOnAction(e->{
            System.out.println("Filter transactions");
            SpendingAccount selectedItem = spendingAccountTable.getSelectionModel().getSelectedItem();
            if(selectedItem != null) {
                transactionTable.setItems(bank.viewAllTransactions(selectedItem));
                transactionTable.refresh();
            }
        });
        addMoney.setOnAction(e->{
            System.out.println("Add money operation on savingaccount");
            SavingAccount selectedItem = savingAccountTable.getSelectionModel().getSelectedItem();
            if(selectedItem != null){
                if((new AccountValidator()).quantityValidation(totalSum.getText()) == 1){
                    SavingAccount newSaving = new SavingAccount(selectedItem.getID(), selectedItem.getInterest());
                    newSaving.setTotalSum(selectedItem.getTotalSum());
                    newSaving.setCreationTime(selectedItem.getCreationTime());
                    if(newSaving.addMoney(Double.parseDouble(totalSum.getText()))){
                        bank.updateAccount(selectedItem, newSaving);
                        savingAccountTable.setItems(bank.viewAllSavingAccount());
                        savingAccountTable.refresh();
                    }
                }
            }
        });
        withDrawmoney.setOnAction(e->{
            System.out.println("Withdraw money operation on savingaccount");
            SavingAccount selectedItem = savingAccountTable.getSelectionModel().getSelectedItem();
            if(selectedItem != null){
               // if((new AccountValidator()).quantityValidation(totalSum.getText()) == 1){
                    SavingAccount newSaving = new SavingAccount(selectedItem.getID(), selectedItem.getInterest());
                    newSaving.setTotalSum(selectedItem.getTotalSum());
                    newSaving.setCreationTime(selectedItem.getCreationTime());
                    if(newSaving.withdrawMoney(0)){
                        bank.updateAccount(selectedItem, newSaving);
                        savingAccountTable.setItems(bank.viewAllSavingAccount());
                        savingAccountTable.refresh();
                    }
                //}
            }
        });
        accountType.setOnAction(e->{
            if(accountType.isSelected()) {
                accountTypeName.setText("Saving Account");
                initializeSavingAccountLeft(bp);
                initializeSavingAccountRight(bp);
            }
            else {
                accountTypeName.setText("Spending Account");
                initializeSpendingAccountLeft(bp);
                initializeSpendingAccountRight(bp);
            }
        });
    }

    private void resetPerson(){
        lastName.clear();
        firstName.clear();
        ssn.clear();
        email.clear();
        telnum.clear();
    }

    private void resetSaving(){
        IDChoice.setValue(IDChoice.getItems().get(0));
        totalSum.clear();
        interestORwithdraw.clear();
    }

    private void resetSpending(){
        if(!IDChoice.getItems().isEmpty())
            IDChoice.setValue(IDChoice.getItems().get(0));
        interestORwithdraw.clear();
        if(!providerCh.getItems().isEmpty())
            providerCh.setValue(providerCh.getItems().get(0));
        if(!recipientCH.getItems().isEmpty())
            recipientCH.setValue(recipientCH.getItems().get(0));

    }

}
