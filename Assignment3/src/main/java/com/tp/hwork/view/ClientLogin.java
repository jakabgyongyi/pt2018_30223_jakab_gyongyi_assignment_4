package com.tp.hwork.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class ClientLogin {
    private TextField lastName;
    private TextField firstName;
    private TextField socialSecurityNumber;
    private Button loginButton;

    public ClientLogin(){
        Stage window = new Stage();
        window.setTitle("Client Login");
        VBox layout = new VBox();
        layout.setAlignment(Pos.TOP_CENTER);
        layout.setSpacing(20);
        initializeFields(layout);
        Scene root = new Scene(layout, 400, 600);
        root.getStylesheets().add("DarkTheme.css");
        window.setScene(root);
        window.show();
    }

    public void handleActionEvent(){
        loginButton.setOnAction(e->{
            System.out.println("Button was pushed");
        });
    }

    private void initializeFields(VBox layout){
        lastName = new TextField();
        lastName.setMaxWidth(250);
        lastName.setPromptText("Gyongyi Aniko");
        firstName = new TextField();
        firstName.setMaxWidth(250);
        firstName.setPromptText("Jakab");
        socialSecurityNumber = new PasswordField();
        socialSecurityNumber.setMaxWidth(250);
        loginButton = new Button("Login");
        ImageView iv = new ImageView(new Image("admin.png"));
        iv.setFitHeight(200);
        iv.setFitWidth(200);
        Label lastNameLabel = new Label("Last Name");
        Label firstNameLabel = new Label("First Name");
        Label passLabel = new Label("Social Security Number");
        layout.getChildren().addAll(iv, lastNameLabel, lastName, firstNameLabel, firstName, passLabel, socialSecurityNumber,loginButton);
    }
}
