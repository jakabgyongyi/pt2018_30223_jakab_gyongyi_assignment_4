package com.tp.hwork.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Timestamp;
import java.util.ArrayList;

public class SpendingAccount extends Account {
    private double withDrawLoss;
    private ArrayList<Transaction> transactions;

    public SpendingAccount(String ID, double withDrawLoss) {
        super(ID);
        this.withDrawLoss = withDrawLoss;
        this.transactions = new ArrayList<>();
    }

    public SpendingAccount(String ID, Timestamp creationTime, double totalSum, double withDrawLoss, ArrayList<Transaction> transactions){
        super(ID, creationTime, totalSum);
        this.withDrawLoss = withDrawLoss;
        this.transactions = transactions;
    }

    public SpendingAccount(){
        super();
        transactions = new ArrayList<>();
    }

    public double getWithDrawLoss() {
        return withDrawLoss;
    }

    public void setWithDrawLoss(double withDrawLoss) {
        this.withDrawLoss = withDrawLoss;
        setChanged();
        notifyObservers("SET WITHDRAWLOSS");
    }

    public ArrayList<Transaction> getTransactions(){
        return transactions;
    }

    public ObservableList<Transaction> getTransactionsObservable(){
        ObservableList<Transaction> transactionsOB = FXCollections.observableArrayList();
        for(Transaction itr: transactions){
            transactionsOB.add(itr);
        }
        return transactionsOB;
    }

    public void setTransactions(ArrayList<Transaction> transactions) {
        this.transactions = transactions;
    }

    public boolean addTransaction(Transaction transaction){
        System.out.println("Adding transaction to account"+this);
        transactions.add(transaction);

        if(transaction.getRecipientID().compareTo(this.getID()) == 0)
            return addMoney(transaction.getQuantity());
        if(transaction.getProviderID().compareTo(this.getID()) == 0)
            return withdrawMoney(transaction.getQuantity());
        return false;
    }

    public boolean addMoney(double Quantity) {
        this.setTotalSum(this.getTotalSum() + Quantity);
        setChanged();
        notifyObservers("ADD MONEY");
        return true;
    }

    public boolean withdrawMoney(double Quantity) {
        if(this.getTotalSum() >= (Quantity * this.getWithDrawLoss() + Quantity)){
            this.setTotalSum(this.getTotalSum() - (Quantity * this.getWithDrawLoss() + Quantity));
            setChanged();
            notifyObservers("WITHDRAW MONEY");
            return true;
        }
        return false;
    }
}
