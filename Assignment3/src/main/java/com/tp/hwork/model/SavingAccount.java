package com.tp.hwork.model;


import java.sql.Timestamp;

public class SavingAccount extends Account {
    private double interest;

    public SavingAccount(String ID, double interest){
        super(ID);
        this.interest = interest;
    }

    public SavingAccount(){
        super();
        this.interest = 0;
    }

    public SavingAccount(String ID, Timestamp creationTime, double totalSum, double interest){
        super(ID, creationTime, totalSum);
        this.interest = interest;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
        setChanged();
        notifyObservers("SET INTEREST");
    }

    public boolean addMoney(double Quantity) {
        if(this.getTotalSum() == 0){
            this.setTotalSum(Quantity);
            setChanged();
            notifyObservers("ADD MONEY");
            return true;
        }
        return false;
    }

    public boolean withdrawMoney(double Quantity) {
        if (this.getTotalSum() != 0) {
            //if(Quantity != this.getTotalSum())
                //return false;
            setChanged();
            notifyObservers("WITHDRAW MONEY");
            this.setTotalSum(0);
            return true;
        }
        return false;
    }
}
