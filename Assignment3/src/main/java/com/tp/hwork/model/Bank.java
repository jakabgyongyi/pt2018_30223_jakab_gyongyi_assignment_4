package com.tp.hwork.model;

import javafx.beans.Observable;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOError;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * @invariant dataBase != null
 * */
public class Bank implements BankProc, Serializable {
    private HashMap<Person, List<Account>> dataBase;

    public Bank(){
        dataBase = new HashMap<>();
    }

    @Override
    public boolean insertPerson(Person person) {
        assert dataBase != null : "Nullpointer exception in insertPerson() method -> bank object";
        assert person != null : "Null pointer exception in insertPerson() method -> person object";
        boolean result;
        if(dataBase.containsKey(person))
            result = false;
        else {
            dataBase.put(person, new ArrayList<>());
            result = true;
        }
        assert result : "This person already exists in bank database";
        return result;
    }

    @Override
    public boolean updatePerson(Person oldPerson, Person newPerson) {
        assert dataBase != null : "Nullpointer exception in updatePerson() method -> bank object";
        assert oldPerson != null : "Nullpointer exception in updatePerson() method -> oldPerson";
        assert newPerson != null : "Nullpointer exception in updatePerson() method -> newPerson";
        boolean result;
        if(!dataBase.containsKey(oldPerson))
            result = false;
        else{
            List<Account> accounts = dataBase.remove(oldPerson);
            dataBase.put(newPerson, accounts);
            result = true;
        }
        assert result : "This person does not exist in Bank database -> cannot be updated";
        return true;
    }

    @Override
    public boolean deletePerson(Person person) {
        assert dataBase != null : "Nullpointer exception in deletePerson() method -> bank object";
        assert person != null : "Nullpointer exception in deletePerson() method";
        boolean result;
        if(!dataBase.containsKey(person))
            result = false;
        else {
            dataBase.remove(person);
            result = true;
        }assert result : "This person does not exist in Bank database -> cannot be deleted";
        return result;
    }

    @Override
    public ObservableList<Person> viewAllPerson() {
        assert dataBase != null : "Nullpointer exception in viewAllPerson() method -> bank object";
        ObservableList<Person> people = FXCollections.observableArrayList();
        for(Person itr: dataBase.keySet()){
            people.add(new Person(itr.getLastName(), itr.getFirstName(), itr.getSocialSecurityNumber(), itr.getEmail(), itr.getTelNumber()));
        }
        return people;
    }

    public ObservableList<String> viewAllSocialSecurityNumber(){
        assert dataBase != null : "Nullpointer exception in viewAllSocialSecurityNumber() method -> bank object";
        ObservableList<String> ids = FXCollections.observableArrayList();
        for(Person itr: dataBase.keySet()){
            ids.add(itr.getSocialSecurityNumber());
        }
        return ids;
    }

    public Person findBySSN(String ssn){
        assert dataBase != null : "Nullpointer exception in findBySSN() method -> bank object";
        for(Person itr: dataBase.keySet()){
            if(itr.getSocialSecurityNumber().compareTo(ssn) == 0)
                return itr;
        }
        return null;
    }

    @Override
    public boolean insertAccount(Person person, Account account) {
        assert dataBase != null : "Nullpointer exception in insertAccount() method -> bank object";
        assert person != null : "Nullpointer exception in insertAccount() method";
        assert account != null : "Nullpointer exception in insertAccount() method";
        boolean result;
        if(!dataBase.containsKey(person))
            result = false;
        else{
            List<Account> accounts = dataBase.remove(person);
            if(accounts.contains(account)) {
                dataBase.put(person, accounts);
                account.addObserver(person);
                account.notifyObservers("Insert account");
                result = false;
            }else{
                dataBase.put(person, accounts);
                account.addObserver(person);
                account.notifyObservers("Insert account");
                accounts.add(account);
                result = true;
            }
        }
        assert result : "The person did not exist in Bank database OR the account already existed";
        return result;
    }

    @Override
    public boolean updateAccount(Account oldAccount, Account newAccount) {
        assert dataBase != null : "Nullpointer exception in updateAccount() method -> bank object";
        assert oldAccount != null : "Nullpointer exception in updateAccount() method";
        assert newAccount != null : "Nullpointer exception in updateAccount() method";
        boolean result = false;
        for(List<Account> itr: dataBase.values()){
            if(itr.contains(oldAccount)){
                /*itr.remove(oldAccount);
                itr.add(newAccount);*/
                oldAccount.setTotalSum(newAccount.getTotalSum());
                if(oldAccount instanceof SavingAccount)
                    ((SavingAccount) oldAccount).setInterest(((SavingAccount)newAccount).getInterest());
                if(oldAccount instanceof SpendingAccount)
                    ((SpendingAccount) oldAccount).setWithDrawLoss(((SpendingAccount)newAccount).getWithDrawLoss());
                result = true;
            }
        }
        assert result : "The account did not exist in Bank database";
        return result;
    }

    @Override
    public boolean deleteAccount(Account account) {
        assert dataBase != null : "Nullpointer exception in deleteAccount() method -> bank object";
        assert account != null : "Nullpointer exception in deleteAccount() method";
        boolean result = false;
        for(List<Account> itr: dataBase.values()){
            if(itr.contains(account)){
                itr.remove(account);
                result = true;
            }
        }
        assert result : "The account did not exist in Bank database -> cannot be deleted";
        return result;
    }

    @Override
    public ObservableList<Account> viewAllAccount() {
        assert dataBase != null : "Nullpointer exception in viewAllAccount() method -> bank object";
        ObservableList<Account> accounts = FXCollections.observableArrayList();
        for(List<Account> itr1: dataBase.values()){
            for(Account itr2: itr1){
                if(itr2 instanceof SavingAccount)
                    accounts.add(itr2);
                if(itr2 instanceof SpendingAccount)
                    accounts.add(itr2);
            }
        }
        return accounts;
    }

    public ObservableList<SavingAccount> viewAllSavingAccount(){
        assert dataBase != null : "Nullpointer exception in viewAllSavingAccount() method -> bank object";
        ObservableList<SavingAccount> accounts = FXCollections.observableArrayList();
        for(List<Account> itr1: dataBase.values()){
            for(Account itr2: itr1){
                if(itr2 instanceof SavingAccount)
                    accounts.add((SavingAccount) itr2);
            }
        }
        return accounts;
    }

    public ObservableList<SpendingAccount> viewAllSpendingAccount(){
        assert dataBase != null : "Nullpointer exception in viewAllSpendingAccount() method -> bank object";
        ObservableList<SpendingAccount> accounts = FXCollections.observableArrayList();
        for(List<Account> itr1: dataBase.values()){
            for(Account itr2: itr1){
                if(itr2 instanceof SpendingAccount)
                    accounts.add((SpendingAccount) itr2);
            }
        }
        return accounts;
    }

    public ObservableList<String> getAllIBAN(String mode){
        assert dataBase != null : "Nullpointer exception in getAllIBAN() method -> bank object";
        assert (mode.compareTo("SAVING") != 0 && mode.compareTo("SPENDING") != 0) : "Mode not valid in getAllIBAN() method";
        ObservableList<String> saving = FXCollections.observableArrayList();
        ObservableList<String> spending = FXCollections.observableArrayList();

        for(List<Account> itr1: dataBase.values()) {
            for (Account itr2 : itr1) {
                if (itr2 instanceof SavingAccount)
                    saving.add(itr2.getID());
                if(itr2 instanceof SpendingAccount)
                    spending.add(itr2.getID());
            }
        }
        if(mode.compareTo("SAVING") == 0)
            return saving;
        return spending;
    }

    public ObservableList<Transaction> viewAllTransactions(SpendingAccount spendingAccount){
        assert dataBase != null : "Nullpointer exception in viewAllTransaction() method -> bank object";
        ObservableList<Transaction> transactions = FXCollections.observableArrayList();
        for(List<Account> itr1: dataBase.values()){
            for(Account itr2: itr1){
                if(itr2.getID().compareTo(spendingAccount.getID()) == 0 && itr2 instanceof SpendingAccount) {
                    transactions = ((SpendingAccount) itr2).getTransactionsObservable();
                }
            }
        }
        return transactions;
    }

    public void addTransaction(Transaction transaction){
        assert dataBase != null : "Nullpointer exception in addTransaction() method -> bank object";
        String providerIBAN = transaction.getProviderID();
        String recipientIBAN = transaction.getRecipientID();
        for(List<Account> itr1: dataBase.values()){
            for(Account itr2: itr1){
                if(itr2 instanceof SpendingAccount){
                    if(itr2.getID().compareTo(providerIBAN) == 0 || itr2.getID().compareTo(recipientIBAN) == 0){
                        if(((SpendingAccount) itr2).addTransaction(transaction))
                            transaction.setDescription("ALL OKAY");
                        else
                            transaction.setDescription("NOT ENOUGH FOUNDATION");
                    }
                }
            }
        }
    }

    public final void writeObject(Object obj) throws IOException{}

    public final void readObject(Object obj)throws IOException, ClassNotFoundException{}
}
