package com.tp.hwork.model;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable {
    protected String ID;
    protected Timestamp creationTime;
    protected double totalSum;

    public Account(String ID){
        this.ID = ID;
        this.creationTime = new Timestamp(System.currentTimeMillis());
        this.totalSum = 0;
    }

    public Account(){
        this.ID = "~";
        this.creationTime = new Timestamp(System.currentTimeMillis());
        this.totalSum = 0;
    }

    public Account(String ID, Timestamp creationTime, double totalSum){
        this.ID = ID;
        this.creationTime = creationTime;
        this.totalSum = totalSum;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
        notifyObservers("SET ID");
    }

    public Timestamp getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
        notifyObservers("SET TIME");
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
        notifyObservers("SET SUM");
    }

    public final void writeObject(Object obj) throws IOException {}

    public final void readObject(Object obj)throws IOException, ClassNotFoundException{}

    public abstract boolean addMoney(double Quantity);
    public abstract boolean withdrawMoney(double Quantity);
}
