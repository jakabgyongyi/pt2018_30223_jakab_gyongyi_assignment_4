package com.tp.hwork.model;

import com.tp.hwork.EmailCreater;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.io.IOException;
import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;

public class Person implements Observer, Serializable {
    private String lastName;
    private String firstName;
    private String socialSecurityNumber;
    private String email;
    private String telNumber;

    public Person(String lastName, String firstName, String socialSecurityNumber, String email, String telNumber) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.email = email;
        this.telNumber = telNumber;
    }

    public Person() {
        this.lastName = "~";
        this.firstName = "~";
        this.socialSecurityNumber = "~";
        this.email = "~";
        this.telNumber = "~";
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Last Name: ").append(lastName).append("\n");
        sb.append("First Name: ").append(firstName).append("\n");
        sb.append("Email: ").append(email).append("\n");
        sb.append("Phone Number: ").append(telNumber).append("\n");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return 50 + ((this.socialSecurityNumber.compareTo("~") == 0) ? 0 : socialSecurityNumber.hashCode());
    }

    @Override
    public boolean equals(Object obj) {
        Person other = (Person) obj;
        if(lastName.compareTo("~") == 0){
            if(other.getLastName().compareTo("~") != 0)
                return false;
        }
        if(firstName.compareTo("~") == 0){
            if(other.getFirstName().compareTo("~") != 0)
                return false;
        }
        if(socialSecurityNumber.compareTo("~") == 0){
            if(other.getSocialSecurityNumber().compareTo("~") != 0)
                return false;
        }
        if(email.compareTo("~") == 0){
            if(other.getEmail().compareTo("~") != 0){
                return false;
            }
        }
        if(telNumber.compareTo("~") == 0){
            if(other.getTelNumber().compareTo("~") != 0){
                return false;
            }
        }
        if(!lastName.equals(other.getLastName()))
            return false;
        if(!firstName.equals(other.getFirstName()))
            return false;
        if(!socialSecurityNumber.equals(other.getSocialSecurityNumber()))
            return false;
        if(!email.equals(other.getEmail()))
            return false;
        if(!telNumber.equals(other.getTelNumber()))
            return false;
        return true;
    }

    public final void writeObject(Object obj) throws IOException {}

    public final void readObject(Object obj)throws IOException, ClassNotFoundException{}

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("We are in person update method");
        /*String from = "jakabgyongyi8@gmail.com";
        String pass = "1997Skorpio!";
        String to = "jakabgyongyi8@gmail.com";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, pass);
            }
        };
        Session session = Session.getInstance(props, auth);
        EmailCreater.sendEmail(session, to, "EuroCluj", "Our system detected a change in one of your account.\nNotification: "+(String)arg);*/
        System.out.println("Change occurred in person: "+this+"Account: "+(Account)o);
    }
}
