package com.tp.hwork.model;

import javafx.collections.ObservableList;

public interface BankProc {
    /**
     * @pre person != null
     * @post @result == true
     */
    public boolean insertPerson(Person person);
    /**
     * @pre oldPerson != null
     * @pre newPerson != null
     * @post @result == true
     */
    public boolean updatePerson(Person oldPerson, Person newPerson);
    /**
     * @pre person != null
     * @post @result == true
     */
    public boolean deletePerson(Person person);
    /**
     * @post @nochange
     */
    public ObservableList<Person> viewAllPerson();
    /**
     * @pre person != null
     * @pre account != null
     * @post @result == true
     */
    public boolean insertAccount(Person person, Account account);
    /**
     * @pre oldAccount != null
     * @pre newAccount != null
     * @post @result == true
     */
    public boolean updateAccount(Account oldAccount, Account newAccount);
    /**
     * @pre account != null
     * @post @result == true
     */
    public boolean deleteAccount(Account account);
    /**
     * @post @nochange
     */
    public ObservableList<Account> viewAllAccount();
}
