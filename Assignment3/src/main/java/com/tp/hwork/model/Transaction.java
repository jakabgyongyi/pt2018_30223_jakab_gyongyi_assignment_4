package com.tp.hwork.model;


import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;

public class Transaction implements Serializable {
    private String providerID;
    private String recipientID;
    private double quantity;
    private Timestamp time;
    private String description;

    public Transaction(){
        super();
    }

    public Transaction(String providerID, String recipientID, double quantity) {
        this.providerID = providerID;
        this.recipientID = recipientID;
        this.quantity = quantity;
        this.time = new Timestamp(System.currentTimeMillis());
    }

    public String getProviderID() {
        return providerID;
    }

    public void setProviderID(String providerID) {
        this.providerID = providerID;
    }

    public String getRecipientID() {
        return recipientID;
    }

    public void setRecipientID(String recipientID) {
        this.recipientID = recipientID;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Provider IBAN: ").append(providerID).append("\n");
        sb.append("Recipient IBAN: ").append(recipientID).append("\n");
        sb.append("Quantity: ").append(quantity).append("\n");
        sb.append("Time of Transaction: ").append(time).append("\n");
        sb.append("Description: ").append(description).append("\n");
        return sb.toString();
    }
    public final void writeObject(Object obj) throws IOException {

    }

    public final void readObject(Object obj)throws IOException, ClassNotFoundException{

    }
}
