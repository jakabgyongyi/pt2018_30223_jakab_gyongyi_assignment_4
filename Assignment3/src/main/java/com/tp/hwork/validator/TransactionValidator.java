package com.tp.hwork.validator;

public class TransactionValidator {

    public boolean ibanCollision(String providerID, String recipientID){
        if(providerID.compareTo(recipientID) == 0)
            return true;
        return false;
    }

    public boolean quantityValidation(String quantity){
        try{
            double quantityDouble = Double.parseDouble(quantity);
            return true;
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
        return false;
    }
}
