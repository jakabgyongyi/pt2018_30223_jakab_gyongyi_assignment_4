package com.tp.hwork.validator;

import javafx.scene.control.TextField;

public class PersonValidator {
    /*-1 -> invalid 0 -> empty 1 -> valid*/

    public int firstNameValidation(TextField firstName){
        String name = firstName.getText();
        if(name.isEmpty())
            return 0;
        String[] splitResult = name.split(" ");
        for(String itr: splitResult){
            if(!itr.matches("[A-Za-z]+")) {
                firstName.setStyle("-fx-text-inner-color: red;");
                return -1;
            }
        }
        return 1;
    }

    public int lastNameValidation(TextField lastName){
        String name = lastName.getText();
        if(name.isEmpty())
            return 0;
        String[] splitResult = name.split("-");
        for(String itr: splitResult){
            if(!itr.matches("[A-Za-z]+")) {
                lastName.setStyle("-fx-text-inner-color: red;");
                return -1;
            }
        }
        return 1;
    }

    public int ssnValidation(TextField ssn){
        String socialsecnumber = ssn.getText();
        if(socialsecnumber.isEmpty())
            return 0;
        if(socialsecnumber.matches("[0-9]+") && socialsecnumber.length() == 13)
            return 1;
        ssn.setStyle("-fx-text-inner-color: red;");
        return -1;
    }

    public int emailValidation(TextField email){
        String mail = email.getText();
        if(mail.isEmpty())
            return 0;
        if(mail.matches("[A-Za-z0-9$!@#^&~.]+"))
            return 1;
        email.setStyle("-fx-text-inner-color: red;");
        return -1;
    }

    public int telNumberValidation(TextField telNumber){
        String number = telNumber.getText();
        if(number.isEmpty())
            return 0;
        if(number.matches("[0-9]+") && number.length() == 10)
            return 1;
        telNumber.setStyle("-fx-text-inner-color: red;");
        return -1;
    }
}
