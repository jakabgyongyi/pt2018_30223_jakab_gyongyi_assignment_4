package com.tp.hwork.validator;

public class AccountValidator {
     /*-1 -> error 0 -> empty input 1 -> valid*/

    public int interestValidation(String interest){
        if(interest.isEmpty())
            return 0;
        try{
            double interestDouble = Double.parseDouble(interest);
            if(interestDouble >= 0.0 && interestDouble < 1.0)
                return 1;
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
        return -1;
    }

    public int withdrawLossValidation(String withdrawLoss){
        if(withdrawLoss.isEmpty())
            return 0;
        try{
            double withdrawDouble = Double.parseDouble(withdrawLoss);
            if(withdrawDouble >= 0.0 && withdrawDouble < 1.0)
                return 1;
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
        return -1;
    }

    public int quantityValidation(String quantity){
        if(quantity.isEmpty())
            return 0;
        try{
            double sumDouble = Double.parseDouble(quantity);
            if(sumDouble > 0.0)
                return 1;
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
        return -1;
    }


}
